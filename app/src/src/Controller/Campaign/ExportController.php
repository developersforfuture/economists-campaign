<?php

namespace App\Controller\Campaign;

use App\Entity\Subscription;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class ExportController extends AbstractController
{
    public function index(ObjectManager $manager): Response
    {
        $subscriptions = $manager->getRepository(Subscription::class)->findAll();
        $response = new StreamedResponse();
        $response->setCallback(function() use ($subscriptions) {
            $handle = fopen('php://output', 'w+');

            // Add the header of the CSV file
            fputcsv($handle, [
                'id',
                'title_before_name',
                'title_after_name',
                'highest_academic_graduation',
                'co_authored_publication_link',
                'staff_page_link',
                'name_email_of_schoolar_nowing_you',
                'first_name',
                'last_name',
                'email_address',
                'graduation_state',
                'publications_state',
                'department', 
                'organization_or_institute',
                'city',
                'state',
                'country',
                'is_verified',
                'verification_token',
                'is_data_policy_accepted',
                'creation_date',
                'verification_date',
            ],',');
            foreach ($subscriptions as $subscription) {
                fputcsv($handle, $subscription->toArray());
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }
}
