<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 * @ORM\Entity(repositoryClass="App\Repository\SubscriptionRepository")
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * First Name (required, published)
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $firstName;
    /**
     * Last Name (required, published)
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $lastName;
    /**
     * Title(s) before your name (optional, published)   	 e.g. Prof. / Dr.
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleBeforeName;
    /**
     * Title(s) after your name (optional, published)      	 e.g. PhD
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleAfterName;
    /**
     * Email Address (required, NOT published)
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $emailAddress;
    /**
     * Highest academic degree in economists or adjacent discipline (required, NOT published)
     * bach./bach./mast./PhD/Dr
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private $highestAcademicDegree;
    /**
     * IF current PhD student and higher
     * Main field of research (required, text field, NOT published)
     * @var string
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $mainFieldOfResearch;
    /**
     * IF current PhD student and higher
     * Publications (required, NOT published) (never, last more than 5 years ago, last less than 5 years ago)
     * @var string
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $publications;
    /**
     * IF PhD student (all beyond required, NOT published)
     * IF NOT Dr./PhD degree: Link to (co-)authored publication
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $coAuthoredPublicationLink;
    /**
     * IF PhD student (all beyond required, NOT published)
     * IF NOT publicat.: Link to staff page with your name
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $staffPageLink;
    /**
     * IF PhD student (all beyond required, NOT published)
     * IF NOT staff page: Name/Email of scholar knowing you
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameEmailOfSchoolarNowingYou;
    /**
     * IF student (required, NOT published)
     * Programme (dropdown: economists, socioeconomists, PPE, Philosophy & Econoimcs, Economic History, Ethics & Economics, economic sociology, Political Economy, Business Administration,  other (--> mit Leerfeld (Name des Studiengangs))
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $programs;
    /**
     * Current status (required, published)
     * Dropdown: Economists / Scholar /  Researcher; Student of Economics / adjacent disciplines; Other (please specify)
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $currentState;
    /**
     * association (required, NOT published)
     * Dropdown: University/Government/Think Tank/Media/School/Other, please detail below
     *
     * @var string[]
     * @ORM\Column(type="array", length=255)
     * @Assert\NotBlank
     */
    private $association;
    /**
     * association details / please specify and optionally add Department or Organisation or Institution (recommended, published)
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $associationDetails;
    /**
     * City (Required)
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;
    /**
     * Country (required, published)
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;
    /**
     * Comments (optional, NOT published)
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comments;
    /**
     * Please inform me about the result of the Economists For Future campaign. (required, NOT published)
     *
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $pleasInform;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $verificationToken;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isDataPolicyAccepted = false;
    /**
     * @var \DateTime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $createdDate;
    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $verificationDate;

    public function __construct()
    {
        $this->firstName = '';
        $this->lastName = '';
        $this->titleBeforeName = '';
        $this->titleAfterName = '';
        $this->emailAddress = '';
        $this->highestAcademicDegree = '';
        $this->mainFieldOfResearch = '';
        $this->publications = '';
        $this->coAuthoredPublicationLink = '';
        $this->staffPageLink = '';
        $this->nameEmailOfSchoolarNowingYou = '';
        $this->programs = '';
        $this->currentState = '';
        $this->association = [];
        $this->associationDetails = '';
        $this->city = '';
        $this->country = '';
        $this->comments = '';
        $this->pleasInform = 0;
        $this->isVerified = 0;
        $this->verificationToken = '';
        $this->isDataPolicyAccepted = 0;
        $this->createdDate = new \DateTime();
        $this->verificationDate = new \DateTime();
    }

    public function toArray(): array
    {
        return [
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->titleBeforeName,
            $this->titleAfterName,
            $this->emailAddress,
            $this->highestAcademicDegree,
            $this->mainFieldOfResearch,
            $this->publications,
            $this->coAuthoredPublicationLink,
            $this->staffPageLink,
            $this->nameEmailOfSchoolarNowingYou,
            $this->programs,
            $this->currentState,
            $this->association,
            $this->associationDetails,
            $this->city,
            $this->country,
            $this->comments,
            $this->pleasInform === false ? 0 : 1,
            $this->isVerified === false ? 0 : 1,
            $this->verificationToken,
            $this->isDataPolicyAccepted === true ? 1 : 0,
            $this->createdDate instanceof \DateTime ? $this->createdDate->format('Y-m-d H:i:s') : '',
            $this->verificationDate instanceof \DateTime ? $this->verificationDate->format('Y-m-d H:i:s') : '',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getTitleBeforeName(): string
    {
        return $this->titleBeforeName;
    }

    /**
     * @param string $titleBeforeName
     */
    public function setTitleBeforeName(string $titleBeforeName): void
    {
        $this->titleBeforeName = $titleBeforeName;
    }

    /**
     * @return string
     */
    public function getTitleAfterName(): string
    {
        return $this->titleAfterName;
    }

    /**
     * @param string $titleAfterName
     */
    public function setTitleAfterName(string $titleAfterName): void
    {
        $this->titleAfterName = $titleAfterName;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress(string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getHighestAcademicDegree(): string
    {
        return $this->highestAcademicDegree;
    }

    /**
     * @param string $highestAcademicDegree
     */
    public function setHighestAcademicDegree(string $highestAcademicDegree): void
    {
        $this->highestAcademicDegree = $highestAcademicDegree;
    }

    /**
     * @return string
     */
    public function getMainFieldOfResearch(): string
    {
        return $this->mainFieldOfResearch;
    }

    /**
     * @param string $mainFieldOfResearch
     */
    public function setMainFieldOfResearch(string $mainFieldOfResearch): void
    {
        $this->mainFieldOfResearch = $mainFieldOfResearch;
    }

    /**
     * @return string
     */
    public function getPublications(): string
    {
        return $this->publications;
    }

    /**
     * @param string $publications
     */
    public function setPublications(string $publications): void
    {
        $this->publications = $publications;
    }

    /**
     * @return string
     */
    public function getCoAuthoredPublicationLink(): string
    {
        return $this->coAuthoredPublicationLink;
    }

    /**
     * @param string $coAuthoredPublicationLink
     */
    public function setCoAuthoredPublicationLink(string $coAuthoredPublicationLink): void
    {
        $this->coAuthoredPublicationLink = $coAuthoredPublicationLink;
    }

    /**
     * @return string
     */
    public function getStaffPageLink(): string
    {
        return $this->staffPageLink;
    }

    /**
     * @param string $staffPageLink
     */
    public function setStaffPageLink(string $staffPageLink): void
    {
        $this->staffPageLink = $staffPageLink;
    }

    /**
     * @return string
     */
    public function getNameEmailOfSchoolarNowingYou(): string
    {
        return $this->nameEmailOfSchoolarNowingYou;
    }

    /**
     * @param string $nameEmailOfSchoolarNowingYou
     */
    public function setNameEmailOfSchoolarNowingYou(string $nameEmailOfSchoolarNowingYou): void
    {
        $this->nameEmailOfSchoolarNowingYou = $nameEmailOfSchoolarNowingYou;
    }

    /**
     * @return string
     */
    public function getPrograms(): string
    {
        return $this->programs;
    }

    /**
     * @param string $programs
     */
    public function setPrograms(string $programs): void
    {
        $this->programs = $programs;
    }

    /**
     * @return string
     */
    public function getCurrentState(): string
    {
        return $this->currentState;
    }

    /**
     * @param string $currentState
     */
    public function setCurrentState(string $currentState): void
    {
        $this->currentState = $currentState;
    }

    /**
     * @return string
     */
    public function getAssociation(): array
    {
        return $this->association;
    }

    /**
     * @param string $association
     */
    public function setAssociation(array $association): void
    {
        $this->association = $association;
    }

    /**
     * @return string
     */
    public function getAssociationDetails(): string
    {
        return $this->associationDetails;
    }

    /**
     * @param string $associationDetails
     */
    public function setAssociationDetails(string $associationDetails): void
    {
        $this->associationDetails = $associationDetails;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getComments(): string
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments(string $comments): void
    {
        $this->comments = $comments;
    }

    /**
     * @return bool
     */
    public function isPleasInform(): bool
    {
        return $this->pleasInform;
    }

    /**
     * @param bool $pleasInform
     */
    public function setPleasInform(bool $pleasInform): void
    {
        $this->pleasInform = $pleasInform;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     */
    public function setIsVerified(bool $isVerified): void
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return string
     */
    public function getVerificationToken(): string
    {
        return $this->verificationToken;
    }

    /**
     * @param string $verificationToken
     */
    public function setVerificationToken(string $verificationToken): void
    {
        $this->verificationToken = $verificationToken;
    }

    /**
     * @return bool
     */
    public function isDataPolicyAccepted(): bool
    {
        return $this->isDataPolicyAccepted;
    }

    /**
     * @param bool $isDataPolicyAccepted
     */
    public function setIsDataPolicyAccepted(bool $isDataPolicyAccepted): void
    {
        $this->isDataPolicyAccepted = $isDataPolicyAccepted;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getVerificationDate(): \DateTime
    {
        return $this->verificationDate;
    }

    /**
     * @param \DateTime $verificationDate
     */
    public function setVerificationDate(\DateTime $verificationDate): void
    {
        $this->verificationDate = $verificationDate;
    }
}
